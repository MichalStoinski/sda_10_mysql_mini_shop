package hibernate.hibernate.util;

import hibernate.shop.User;
import hibernate.shop.UserRepository;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.Optional;

/*
    create 2018-04-10 20:44 by Michał
*/
public class UserSessionHelper {

    /**
     * get user from email from cookie
     *
     * @param cookies array of cookies
     * @return optional of user
     */
    public static Optional<User> getUserFromCookie(Cookie[] cookies) {
        if (cookies != null) {
            Optional<Cookie> cookieFromEmail = Arrays.stream(cookies)
                    .filter(cookie -> cookie.getName().equals("email"))
                    .findFirst();
            if (cookieFromEmail.isPresent()) {
                return UserRepository.findByEmail(cookieFromEmail.get().getValue());
            }
        }
        return Optional.empty();
    }
}
