/*
package hibernate;

import hibernate.shop.*;

import java.math.BigDecimal;
import java.util.*;

public class App {

    private static final String LACK = "lack";

    public static void main(String[] args) {

        //TODO change all method calling into tests

        System.out.println("=======================================");
        System.out.println("============== NEW PRODUCTS ===========");
        System.out.println("=======================================");

        ProductRepository.saveProductIntoDB(new Product("Mercedes S", ProductType.CAR,
                new Price(new BigDecimal(600_000), new BigDecimal(500_000))));
        ProductRepository.saveProductIntoDB(new Product("Mercedes A", ProductType.CAR,
                new Price(new BigDecimal(400_000), new BigDecimal(300_000))));
        ProductRepository.saveProductIntoDB(new Product("red car", ProductType.TOY,
                new Price(new BigDecimal(50), new BigDecimal(40))));

        Optional<Product> prod1 = ProductRepository.findOneById(1L);
        Optional<Product> prod2 = ProductRepository.findOneById(2L);
        Optional<Product> prod3 = ProductRepository.findOneById(3L);

        //replace lambda expression with method reference
        System.out.println("::: " + prod1.map(Product::getName).orElse(LACK));
        System.out.println("::: " + prod2.map(Product::getName).orElse(LACK));
        System.out.println("::: " + prod3.map(Product::getName).orElse(LACK));

        ProductRepository.findAll().forEach(p -> System.out.println(":::findAll " + p.getName()));
        ProductRepository.findAllNative().forEach(p -> System.out.println(":::native: " + p.getName()));
        ProductRepository.findAllByProductType(ProductType.CAR)
                .forEach(p -> System.out.println(":::findAllCARs " + p.getName()));
        ProductRepository.findAllByProductType(ProductType.TOY)
                .forEach(p -> System.out.println(":::findAllTOYs " + p.getName()));

        Long carCount = ProductRepository.countProductType(ProductType.CAR);
        System.out.println(":::Number of CARs in db: " + carCount);

        BigDecimal grossPrice = new BigDecimal(500);
        ProductRepository.findAllWithPriceLess(grossPrice).forEach(p -> System.out.println("Price < " + grossPrice
                + ": " + p.getName() + ", cena: " + p.getPrice().getGrossPrice()));

        ProductRepository.findAllByNameLike("merc")
                .forEach(p -> System.out.println(":::Mercedes'es (with query): " + p.getName()));

        ProductRepository.findAllByNameLikeWithCriteria("merc")
                .forEach(p -> System.out.println(":::Mercedes'es (with criteria): " + p.getName()));

        System.out.println(":::cheap cars and toys");

        ProductRepository.findAllToysAndCarsCostLess(grossPrice)
                .forEach(p -> System.out.println(":::find with Query: " + p.getName()));

        ProductRepository.findAllToysAndCarsCostLessWithCriteria(grossPrice)
                .forEach(p -> System.out.println(":::criteria find: " + p.getName()));

        //increase both price (net and gross) by one - example of getting and setting value
        Optional<Product> optionalProduct = ProductRepository.findOneById(3L);
        if (optionalProduct.isPresent()) {
            Product toy = optionalProduct.get();
            toy.getPrice().setGrossPrice(toy.getPrice().getGrossPrice().add(BigDecimal.ONE));
            toy.getPrice().setNetPrice(toy.getPrice().getNetPrice().add(BigDecimal.ONE));
            ProductRepository.saveProductIntoDB(toy);
        }

        //TODO change calling deleting method into single test
//        ProductRepository.deleteProductById(2L);

        System.out.println("=======================================");
        System.out.println("=============== NEW ORDER =============");
        System.out.println("=======================================");

        OrderItem orderItem1 = OrderItem.builder()
                .amount(BigDecimal.ONE)
                .product(prod1.get())
                .price(prod1.get().getPrice())
                .build();

        OrderItem orderItem2 = OrderItem.builder()
                .amount(new BigDecimal(9))
                .product(prod2.get())
                .price(prod1.get().getPrice())
                .build();

        OrderItem orderItem3 = OrderItem.builder()
                .amount(new BigDecimal(5))
                .product(prod3.get())
                .price(prod1.get().getPrice())
                .build();

        //set basic data (common) od new order
        Order order = Order.builder()
                .userEmail("test@sda.pl")
                .totalGross(BigDecimal.ZERO)
                .totalNet(BigDecimal.ZERO)
                .orderItemSet(new HashSet<>())
                .build();

        //Adding items (products) to order
        order.addOrderItem(orderItem1);
        order.addOrderItem(orderItem2);
        order.addOrderItem(orderItem3);

        BigDecimal totalGross = BigDecimal.ZERO;
        BigDecimal totalNet = BigDecimal.ZERO;
        for (OrderItem item : order.getOrderItemSet()) {
            totalGross = totalGross.add(item.getPrice().getGrossPrice().multiply(item.getAmount()));
            totalNet = totalNet.add(item.getPrice().getNetPrice().multiply(item.getAmount()));
        }
        order.setTotalGross(totalGross);
        order.setTotalNet(totalNet);
        OrderRepository.saveOrderIntoDB(order);
        System.out.println(":::saved in db, with ud: " + order.getId());


        System.out.println("=======================================");
        System.out.println("============ FINDING ORDER ============");
        System.out.println("=======================================");

        long productId = 1L;
        OrderRepository.findAllOrdersWithProduct(productId)
                .forEach(o -> System.out.println(":::Order with product id=" + productId + ": " + o.getId()));

        //something like "other customers buy also"
        List<Order> allOrdersWithProduct = OrderRepository.findAllOrdersWithProduct(productId);
        for (Order o : allOrdersWithProduct) {
            o.getOrderItemSet().forEach(
                    item -> System.out.println(":::Order id: " + o.getId() + " : " + item.getProduct().getName()));
        }

        //the same as above, but without order object
        OrderRepository.findAllOrdersWithProduct(productId)
                .forEach(o -> o.getOrderItemSet()
                        .forEach(orderItem -> System.out.println(orderItem.getProduct().getName())));

        System.out.println("=======================================");
        System.out.println("========= NEW ORDERS COMPLAINT ========");
        System.out.println("=======================================");

        Set<Order> orderSet = new HashSet<>();
        orderSet.add(order); //can be many orders...

        OrdersComplaint newComplaint = OrdersComplaint.builder()
                .complaintStatus(ComplaintStatus.PENDING)
                .message("New complaint from John")
                .orderSet(orderSet)
                .build();

        OrderComplaintRepository.saveOrderComplaintIntoDB(newComplaint);
        System.out.println(":::saved in db, with ud: " + newComplaint.getId());


        System.out.println("=======================================");
        System.out.println("=============== NEW CART ==============");
        System.out.println("=======================================");

        Cart cart = Cart.builder()
                .userEmail("user@sda.pl")
                .cartItemSet(new HashSet<>())
                .cartItemSet(Collections.emptySet())
                .build();

        CartItem cartItem1 = CartItem.builder()
                .amount(new BigDecimal(2))
                .product(prod1.get())
                .price(prod1.get().getPrice())
                .build();

        CartItem cartItem2 = CartItem.builder()
                .amount(new BigDecimal(3))
                .product(prod3.get())
                .price(prod3.get().getPrice())
                .build();

        cart.addCartItem(cartItem1);
        System.out.println(":::price " + cart.getPrice().getGrossPrice());
        cart.addCartItem(cartItem2);
        System.out.println(":::price " + cart.getPrice().getGrossPrice());

        CartRepository.saveCartIntoDB(cart);
        System.out.println(":::saved in db, with ud: " + cart.getId());

    }
}
*/
