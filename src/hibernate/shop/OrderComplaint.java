package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data //auto-create setters
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(exclude = "orderSet")
public class OrderComplaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String message;

    @Enumerated(value = EnumType.STRING)
    ComplaintStatus complaintStatus;

    @ManyToMany(mappedBy = "ordersComplaintSet")
    Set<Order> orderSet;

}
