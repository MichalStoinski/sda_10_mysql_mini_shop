package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/*
    create 2018-03-17 09:45 by Michał
*/

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//exclude one to many relations object to break the infinite recursion
@EqualsAndHashCode(exclude = {"cartItemSet", "user"})
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @OneToOne
    User user;
    @Transient
    Price price;

    //one cart can contain many different products
    //@attribute mappedBy set the owner of relation
    //@attribute cascade forces saving this entity with depended entity
    //@attribute fetch
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<CartItem> cartItemSet;

    void addCartItem(CartItem cartItem) {
        cartItem.setCart(this);
        this.cartItemSet.add(cartItem);
    }

    public BigDecimal getTotalGross() {
/*
        double sum = cartItemSet
                .stream()
                .mapToDouble(value -> value.getAmount().multiply(value.getPrice().getGrossPrice()).doubleValue())
                .sum();
*/
        return cartItemSet
                .stream().map(cartItem -> cartItem.getAmount().multiply(cartItem.getPrice().getGrossPrice()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalNet() {
        return cartItemSet
                .stream().map(cartItem -> cartItem.getAmount().multiply(cartItem.getPrice().getNetPrice()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public boolean isEmpty(){
        return this.cartItemSet == null || this.cartItemSet.size() == 0;
    }
}
