package hibernate.shop;

public enum ComplaintStatus {
    PENDING, REJECT, APROVED
}
