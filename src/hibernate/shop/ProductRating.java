package hibernate.shop;

/*
    create 2018-04-14 11:04 by Michał
*/

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    double rating;
    String description;
    LocalDateTime addTimeStamp;

    @ManyToOne
    User user;

    @ManyToOne
    Product product;

    boolean isVisible;
}
