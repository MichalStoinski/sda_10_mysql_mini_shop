package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
@AllArgsConstructor
@Builder
@Data //gives getters and setters
@NoArgsConstructor
//exclude orderItemSet because
@EqualsAndHashCode(exclude = {"orderItemSet", "productRatings"})
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private LocalDate date;
    @Enumerated
    private ProductType productType;
    @Embedded
    private Price price;
    private String description;

    //set has only unique items
    @OneToMany(mappedBy = "product")
    Set<OrderItem> orderItemSet;

    @OneToMany(mappedBy = "product")
    Set<ProductRating> productRatings;

    @Lob
    private byte[] image;

    public Product(String name, ProductType productType, Price price) {
        this.name = name;
        this.productType = productType;
        this.price = price;
        this.date = LocalDate.now();
    }

}
