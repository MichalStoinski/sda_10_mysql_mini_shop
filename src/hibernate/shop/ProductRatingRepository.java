package hibernate.shop;

import hibernate.hibernate.util.RepositoryUtil;

/*
    create 2018-04-14 11:15 by Michał
*/
public class ProductRatingRepository {

    public static void saveProductRatingIntoDB(ProductRating productRating) {
        RepositoryUtil<ProductRating> productRatingRepositoryUtil = new RepositoryUtil<>();
        productRatingRepositoryUtil.saveObjectIntoDB(productRating);
    }

}
