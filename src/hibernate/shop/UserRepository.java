package hibernate.shop;

import hibernate.hibernate.util.HibernateUtil;
import hibernate.hibernate.util.RepositoryUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Optional;

/*
    create 2018-04-09 21:30 by Michał
*/

public class UserRepository {

    /**
     * Use method from utility class
     *
     * @param user save into db
     */
    public static void saveUser(User user) {
        RepositoryUtil<User> userRepositoryUtil = new RepositoryUtil<>();
        userRepositoryUtil.saveObjectIntoDB(user);
    }

    public static Optional<User> findByEmailAndPassword(String email, String password) {
        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT u FROM User u WHERE u.email = :email AND u.password = :password";
            Query query = session.createQuery(jpql);
            query.setParameter("email", email);
            query.setParameter("password", password);
            //TODO why casting type is necessary here?
            return Optional.ofNullable((User) query.getSingleResult());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static Optional<User> findByEmail(String email) {
        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT u FROM User u WHERE u.email = :email";
            Query query = session.createQuery(jpql);
            query.setParameter("email", email);
            //TODO why casting type is necessary here?
            return Optional.ofNullable((User) query.getSingleResult());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
