package hibernate.shop.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/*
    create 2018-04-07 11:29 by Michał
*/
public class HelloServlet extends HttpServlet {

    /**
     * @param request  data from client (web browser), eg. parameters, form data, cookies
     * @param response data to send to client (web browser), eg. html code, redirecting
     * @throws IOException require method getWriter
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.write("<html><head></head><body><h1>Hello world</h1></body></html>");
    }
}
