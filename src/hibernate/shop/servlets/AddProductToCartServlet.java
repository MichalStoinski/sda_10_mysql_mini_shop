package hibernate.shop.servlets;

import hibernate.hibernate.util.UserSessionHelper;
import hibernate.shop.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;

/*
    create 2018-04-10 14:24 by Michał
*/
public class AddProductToCartServlet extends HttpServlet {

    private static final int BIGGER = 1;
    private static final int SMALLER_OR_EQUAL = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        PrintWriter writer = resp.getWriter();
        String result;

        Optional<User> user = UserSessionHelper.getUserFromCookie(req.getCookies());
        Long productId = MathUtil.parseStringToLong(req.getParameter("productId"));
        BigDecimal productAmount = MathUtil.parseStringToBigDecimal(req.getParameter("productAmount"));

        if (user.isPresent() && productId > 0 && productAmount.compareTo(BigDecimal.ZERO) == BIGGER) {

            Optional<Cart> cartByUserId = CartRepository.findByUserId(user.get().getId());
            Cart cart;

            if (cartByUserId.isPresent()) {
                //user has a cart
                cart = cartByUserId.get();
            } else {
                //user has no cart - create new cart for him
                cart = new Cart();
                cart.setCartItemSet(new HashSet<>());
                cart.setUser(user.get());
            }

            //check if has a product inside
            Optional<CartItem> productInCart = cart.getCartItemSet()
                    .stream()
                    .filter(cartItem -> cartItem.getProduct().getId().equals(productId))
                    .findFirst();
            if (productInCart.isPresent()) {
                productInCart.get().setAmount(productInCart.get().getAmount().add(productAmount));
                CartRepository.saveCartIntoDB(cart);
                result = "Dodano " + productAmount + " sztuk produktu " + productId;
            } else {
                boolean addingResult = CartRepository.createNewCartItem(cart, productId, productAmount);
                if (addingResult) {
                    CartRepository.saveCartIntoDB(cart);
                    result = "Dodano " + productAmount + " sztuk produktu " + productId;
                } else {
                    result = "Nie ma produktu o id: " + productId;
                }
            }
        } else {
            //exceptions part
            result = "Nieznany błąd. produkt nie został dodany";
            if (!user.isPresent()) {
                result = "Nie można dodać produktu. Proszę się zalogować.";
            }
            if (productId >= 0) {
                result = "Nie ma produktu o id: " + productId;
            }
            if (productAmount.compareTo(BigDecimal.ZERO) <= SMALLER_OR_EQUAL) {
                result = "Nie można dodać takiej ilości produktu. Liczba musi być większa niż 0.";
            }
        }

        req.getRequestDispatcher("/cart.jsp").forward(req, resp);
//        writer.write(result);
    }
}
