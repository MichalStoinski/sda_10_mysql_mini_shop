package hibernate.shop.servlets;

public enum OrderStatus {
    NEW, CANCELED, PANDING, PENDING, WAITING_FOR_PAYMENT, PAID, DONE
}
