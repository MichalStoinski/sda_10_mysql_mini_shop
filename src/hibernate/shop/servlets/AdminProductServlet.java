package hibernate.shop.servlets;

import hibernate.shop.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;

/*
    create 2018-04-21 14:46 by Michał
*/

@WebServlet(name = "AdminProductServlet", urlPatterns = "/editOrAddProduct")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
        maxFileSize=1024*1024*10,      // 10MB
        maxRequestSize=1024*1024*50)   // 50MB
public class AdminProductServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        Long id = MathUtil.parseStringToLong(req.getParameter("id"));
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String productType = req.getParameter("productType");
        BigDecimal netPrice = MathUtil.parseStringToBigDecimal(req.getParameter("netPrice"));
        BigDecimal grossPrice = MathUtil.parseStringToBigDecimal(req.getParameter("grossPrice"));

        Product product = Product.builder()
                .date(LocalDate.now())
                .name(name)
                .description(description)
                .productType(ProductType.valueOf(productType))
                .price(new Price(grossPrice, netPrice))
                .build();
        if (id > 0) {
            product.setId(id);
        }

        InputStream input = req.getPart("image").getInputStream();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[10240];
        for (int length = 0; (length = input.read(buffer)) > 0; ) output.write(buffer, 0, length);
        product.setImage(output.toByteArray());

        ProductRepository.saveProductIntoDB(product);
    }
}
