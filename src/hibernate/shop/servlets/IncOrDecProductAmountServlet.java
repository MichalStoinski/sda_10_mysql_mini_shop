package hibernate.shop.servlets;

import hibernate.hibernate.util.UserSessionHelper;
import hibernate.shop.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Optional;

/*
    create 2018-04-11 10:55 by Michał
*/
public class IncOrDecProductAmountServlet extends HttpServlet {

    private static final int BIGGER = 1;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        Optional<User> userFromCookie = UserSessionHelper.getUserFromCookie(req.getCookies());
        Long productId = MathUtil.parseStringToLong(req.getParameter("productId"));
        String method = req.getParameter("method");
        PrintWriter writer = resp.getWriter();

        if (userFromCookie.isPresent()) {
            Optional<Cart> cartByUserId = CartRepository.findByUserId(userFromCookie.get().getId());
            if (cartByUserId.isPresent()) {
                Optional<CartItem> itemFromCart = cartByUserId.get()
                        .getCartItemSet()
                        .stream()
                        .filter(cartItem -> cartItem.getProduct().getId().equals(productId))
                        .findFirst();
                if (itemFromCart.isPresent()) {
                    CartItem item = itemFromCart.get();
                    switch (method) {
                        case "add":
                            item.setAmount(item.getAmount().add(BigDecimal.ONE));
                            break;
                        case "subtract":
                            if (item.getAmount().compareTo(BigDecimal.ONE) == BIGGER) {
                                item.setAmount(item.getAmount().subtract(BigDecimal.ONE));
                            } else {
                                cartByUserId.get().getCartItemSet().remove(item);
                            }
                            break;
                        default:
                            String result = "Błąd. Nieznany parametr \"" + method + "\"";
                            writer.write(result);
                            break;
                    }
                    CartRepository.saveCartIntoDB(cartByUserId.get());
                }
            }
        }
        req.getRequestDispatcher("/cart.jsp").forward(req, resp);
    }
}
