package hibernate.shop;

import java.math.BigDecimal;

/*
    create 2018-04-11 05:19 by Michał
*/
public class MathUtil {

    /**
     * Don't return null
     *
     * @param productAmount String to convert
     * @return value type BigDecimal
     */
    public static BigDecimal parseStringToBigDecimal(String productAmount) {
        //todo should not return null
        try {
            return new BigDecimal(productAmount);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    /**
     * Don't return null
     *
     * @param productId String to convert
     * @return value type Long
     */
    public static Long parseStringToLong(String productId) {
        //todo should not return null
        try {
            return Long.valueOf(productId);
        } catch (NumberFormatException e) {
            return 0L;
        }
    }

    /**
     * Don't return null
     *
     * @param rating String to convert
     * @return value type double
     */
    public static double parseStringToDouble(String rating) {
        try {
            return new Double(rating);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
}
