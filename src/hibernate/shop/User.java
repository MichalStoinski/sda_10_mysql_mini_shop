package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/*
    create 2018-04-09 21:24 by Michał
*/

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"productRatings", "orders"})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String email;
    String password;
    String firstName;
    String lastName;

    @OneToOne(mappedBy = "user")
    Cart cart;

    @OneToMany(mappedBy = "user")
    Set<ProductRating> productRatings;

    @OneToMany(mappedBy = "user")
    Set<Order> orders;
}
