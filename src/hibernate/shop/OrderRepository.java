package hibernate.shop;

import hibernate.hibernate.util.HibernateUtil;
import hibernate.hibernate.util.RepositoryUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class OrderRepository {

    public static void saveOrderIntoDB(Order order) {
        RepositoryUtil<Order> orderRepositoryUtil = new RepositoryUtil<>();
        orderRepositoryUtil.saveObjectIntoDB(order);
    }

    public static Optional<Order> findOrder(Long id){

        //TODO why it's necessary to assign null
        Session session = null;
        try {
            session = HibernateUtil.openSession();
//            Optional<Order> order = Optional.ofNullable(session.find(Order.class, id));
            String hql = "SELECT o FROM Order o LEFT JOIN FETCH o.orderItemSet oi WHERE o.id = :id";
            Query query = session.createQuery(hql);
            query.setParameter("id",id);
            Optional<Order> order = Optional.ofNullable((Order) query.getSingleResult());
            return order;
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }

    }

    public static List<Order> findAll() {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT o FROM Order o LEFT JOIN FETCH o.orderItemSet od";
            Query query = session.createQuery(jpql);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Order> findAllByEmail(Long userId, int offset) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT o FROM Order o WHERE o.user.id = :userId ORDER BY o.id DESC";
            Query query = session.createQuery(jpql);
            query.setParameter("userId", userId);
            //show 15 orders per page
            query.setMaxResults(15);
            query.setFirstResult(offset);
            query.getResultList();
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Order> findAllOrdersWithProduct(Long productId) {
        Session session = null;

        try {
            session = HibernateUtil.openSession();
            //TODO another SQL query
            String jpql = "SELECT o FROM Order o LEFT JOIN FETCH o.orderItemSet od WHERE od.product.id = :id";
//          String jpql = "SELECT o FROM OrderItem od LEFT JOIN od.order o WHERE od.product.id = :id";
            Query query = session.createQuery(jpql);
            query.setParameter("id", productId);
            return query.getResultList();

        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

}
