package hibernate.shop;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.math.BigDecimal;

@NoArgsConstructor
@Data
@Embeddable
public class Price {
    //TODO check if this field could bo private
    private BigDecimal grossPrice;
    private BigDecimal netPrice;

    @Transient
    BigDecimal vatPrice;

    public Price(BigDecimal grossPrice, BigDecimal netPrice) {
        this.grossPrice = grossPrice;
        this.netPrice = netPrice;
    }
}
