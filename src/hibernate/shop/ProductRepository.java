package hibernate.shop;

import hibernate.hibernate.util.HibernateUtil;
import hibernate.hibernate.util.RepositoryUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ProductRepository {

    public static void saveProductIntoDB(Product product) {
        RepositoryUtil<Product> productRepositoryUtil = new RepositoryUtil<>();
        productRepositoryUtil.saveObjectIntoDB(product);
    }
    
    public static void deleteProductById(Long id) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            session.getTransaction().begin();
            Optional<Product> productById = findOneById(id);
            if (productById.isPresent()) {
                session.delete(productById.get());
            } else {
                throw new NoSuchElementException("Product not found in db");
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Product> findAll() {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT p FROM Product p";
            Query query = session.createQuery(jpql);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Product> findAllByNameLike(String name) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT p FROM Product p WHERE UPPER(p.name) like :name";
            Query query = session.createQuery(jpql);
            query.setParameter("name", "%" + name.toUpperCase() + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Product> findAllToysAndCarsCostLess(BigDecimal grossPrice) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT p FROM Product p WHERE " +
                    "(p.productType = :typeCar OR p.productType = :typeToy)" +
                    "AND p.price.grossPrice < :price";
            Query query = session.createQuery(jpql);
            query.setParameter("price", grossPrice);
            query.setParameter("typeCar", ProductType.CAR);
            query.setParameter("typeToy", ProductType.TOY);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Product> findAllByNameLikeWithCriteria(String name) {
        Session session = null;
        try {
            //open session to database
            session = HibernateUtil.openSession();
            //create CriteriaBuilder
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Product> query = cb.createQuery(Product.class);
            //define entity, to get from
            Root<Product> from = query.from(Product.class);
            query.select(from);
            Predicate whereNameLike = cb.like(from.get("name"), "%" + name.toUpperCase() + "%");
            CriteriaQuery<Product> criteriaQuery = query.where(whereNameLike);

            return session.createQuery(criteriaQuery).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    public static List<Product> findAllToysAndCarsCostLessWithCriteria(BigDecimal grossPrice) {
        Session session = null;
        try {
            //open session to database
            session = HibernateUtil.openSession();
            //create CriteriaBuilder
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Product> query = cb.createQuery(Product.class);
            //define entity, to get from
            Root<Product> from = query.from(Product.class);
            query.select(from);
            Predicate toyType = cb.equal(from.get("productType"), ProductType.TOY);
            Predicate carType = cb.equal(from.get("productType"), ProductType.CAR);
            Predicate lessThan = cb.lessThan(from.get("grossPrice"), grossPrice);

            Predicate toyOrCar = cb.or(toyType, carType);
            Predicate toyOrCarPriceLess = cb.and(toyOrCar, lessThan);
            CriteriaQuery<Product> criteriaQuery = query.where(toyOrCarPriceLess);
            return session.createQuery(criteriaQuery).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    public static List<Product> findAllWithPriceLess(BigDecimal maxGrossPrice) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT p FROM Product p WHERE p.price.grossPrice < :price";
            Query query = session.createQuery(jpql);
            query.setParameter("price", maxGrossPrice);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static List<Product> findAllByProductType(ProductType productType) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT p FROM Product p WHERE p.productType = :productType";
            Query query = session.createQuery(jpql);
            query.setParameter("productType", productType);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static Long countProductType(ProductType productType) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT COUNT(p.id) FROM Product p WHERE p.productType = :productType";
            Query query = session.createQuery(jpql);
            query.setParameter("productType", productType);
            return (Long) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public static Optional<Product> findOneById(Long id) {
        Session session = null;
        try {
            session = HibernateUtil.openSession();
            Product product = session.find(Product.class, id);
            return Optional.ofNullable(product);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    public static List<Product> findAllNative() {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String sql = "SELECT * FROM product AS p";
            Query query = session.createNativeQuery(sql, Product.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }


}

