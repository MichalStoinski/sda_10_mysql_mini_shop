package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

@Entity
@Table(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"orderItemSet", "ordersComplaintSet", "orderHistorySet"})
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    BigDecimal totalNet;
    BigDecimal totalGross;
    @ManyToOne
    @JoinColumn
    User user; //change from user email

    //One order can have many different products
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<OrderItem> orderItemSet;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<OrderHistory> orderHistorySet;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "order_complaint_history",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "order_complaint_id", referencedColumnName = "id")
    )
    Set<OrderComplaint> ordersComplaintSet;

    //constructor used in query in OrderRepository class
    public Order(BigDecimal totalGross, User user) {
        this.totalGross = totalGross;
        this.user = user;
    }

    //adding item into this order
    public void addOrderItem(OrderItem orderItem) {
        orderItem.setOrder(this);
        this.orderItemSet.add(orderItem);
    }

    //adding status history into this order
    public void addOrderHistory(OrderHistory orderHistory) {
        orderHistory.setOrder(this);
        this.orderHistorySet.add(orderHistory);
    }

    public OrderHistory getCurrentOrderHistory() {
        return this
                .getOrderHistorySet()
                .stream().min(Comparator.comparing(OrderHistory::getId))
                .orElse(new OrderHistory());
    }
/*
        return this
                .getOrderHistorySet()
                .stream()
                .sorted((o1, o2) -> o1.getId().compareTo(o2.getId()))
                .findFirst()
                .orElse(new OrderHistory());
    }
*/
}
