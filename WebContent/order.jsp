<%@ page import="hibernate.shop.MathUtil" %>
<%@ page import="hibernate.shop.Order" %>
<%@ page import="hibernate.shop.OrderRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Item - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<%@include file="head.jsp" %>

<%
    String orderId = request.getParameter("orderId");
    Optional<Order> order = OrderRepository.findOrder(MathUtil.parseStringToLong(orderId));

    if (order.isPresent() && userFromCookie.isPresent() && order.get().getUser().getId().equals(userFromCookie.get().getId())) {
        pageContext.setAttribute("order", order.get());
        pageContext.setAttribute("orderItems", order.get().getOrderItemSet());
    }

%>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!--Left Menu -->
        <%@include file="leftMenu.jsp" %>

        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <div class="card-body">
                <h3 class="card-title">Zamówienie numer ${order.id}</h3>
                <h4>kwota ${order.totalGross} zł</h4>
                <h5>netto ${order.totalNet} zł</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit
                    fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium
                    animi perspiciatis molestias iure, ducimus!</p>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Lp.</th>
                    <th scope="col">Nazwa produktu</th>
                    <th scope="col">Ilość</th>
                    <th scope="col">Kwota netto</th>
                    <th scope="col">Kwota brutto</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orderItems}" var="item" varStatus="it">
                    <tr>
                        <th scope="row">${it.index+1}</th>
                        <td><a href="/product.jsp?productId=${item.product.id}">${item.product.name}</a></td>
                        <td>${item.amount}</td>
                        <td>@twitter</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <!-- /.card -->


        </div>
        <!-- /.col-lg-9 -->

    </div>

</div>
<!-- /.container -->

<!-- Footer -->
<%@include file="footer.jsp" %>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
