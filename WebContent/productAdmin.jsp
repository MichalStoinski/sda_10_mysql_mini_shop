<%@ page import="hibernate.shop.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Portfolio Item - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">

    <!-- rating -->
    <!-- default styles -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>

    <!-- optionally if you need to use a theme, then include the theme CSS file as mentioned below -->
    <link href="themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css"/>

    <!-- important mandatory libraries -->
    <script src="js/jquery.js"></script>
    <script src="js/star-rating.js" type="text/javascript"></script>

    <!-- optionally if you need to use a theme, then include the theme JS file as mentioned below -->
    <script src="themes/krajee-svg/theme.js"></script>

</head>
<!-- Java code -->
<%
    Optional<Product> productById = ProductRepository
            .findOneById(MathUtil.parseStringToLong(request.getParameter("productId")));

    if (productById.isPresent()) {
        pageContext.setAttribute("product", productById.get());
        //productById.ifPresent(product -> pageContext.setAttribute("product", product));
    } else {
        Product product = new Product();
        product.setPrice(new Price());
        pageContext.setAttribute("product", new Product());
    }

    ProductType[] productTypes = ProductType.values();
    pageContext.setAttribute("productTypeList", productTypes);
%>

<body>

<!-- Navigation -->
<%@include file="head.jsp" %>

<!-- Page Content -->
<div class="container">
    <form method="post" action="/editOrAddProduct" enctype="multipart/form-data">
        <div class="form-group">
            <label>Nazwa produktu</label>
            <input type="text" name="name" value="${product.name}">
            <input type="hidden" name="id", value="${product.id}">
        </div>
        <div class="form-group">
            <label>Typ produktu</label>
            <select name="productType">
                <c:forEach items="${productTypeList}" var="pd">
                    <c:if test="${pd.equals(product.productType)}">
                        <option selected="selected">${pd.name()}</option>
                    </c:if>
                    <c:if test="${pd.equals(product.productType)}">
                        <option>${pd.name()}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label>Cena netto:</label>
            <input type="number" name="netPrice" value="${product.price.netPrice}">
        </div>
        <div>
            <label>Cena brutto:</label>
            <input type="number" name="grossPrice" value="${product.price.grossPrice}">
        </div>
        <div class="form-group">
            <label>Opis produktu:</label>
            <textarea name="description"></textarea>
        </div>
        <div class="form-group">
            <input type="file" name="image">
        </div>

        <div class="form-group">
            <button type="submit">Zapisz</button>
        </div>


    </form>
</div>
<!-- /.container -->

<!-- Footer -->
<%@include file="footer.jsp" %>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
